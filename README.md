# ISS application - Andrejko

## How to contribute 
- hello world

## Docs

### Console Menu 
1) Show current ISS position
2) Calculate ISS speed 
3) List of people in the space
4) Exit application 

### Used technologies 
- Hibernate 
- Guice
- Flyway
- Lombok 
- SL4J
- Jackson

## References
- TBD