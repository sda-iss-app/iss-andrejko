package com.sda.andrejko.iss;

import ch.qos.logback.classic.Logger;
import com.sda.andrejko.iss.console.IssConsoleApplication;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.flywaydb.core.Flyway;


public class Application {

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(Application.class);


    public static void main(String[] args) throws IOException {

        // We need to load properties, and configure flyway
        // config is in resources/flyway.properties
        Properties properties = new Properties();
        InputStream resourceAsStream = Application.class.getClassLoader().getResourceAsStream("flyway.properties");
        properties.load(resourceAsStream);

        // initialize flyway and run it
        // flyway will update DB schema according to migration scripts
        // migration script are in resources/db/migration
        Flyway flyway = Flyway.configure().configuration(properties).load();
        flyway.migrate();

        // after we successfully updated/created DB, we can run our APP
        LOGGER.info("Starting ISS APP..."); //trace, debug, info, warm, error
        IssConsoleApplication.run();
    }
}
