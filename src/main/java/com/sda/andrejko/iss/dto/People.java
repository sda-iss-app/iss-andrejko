package com.sda.andrejko.iss.dto;

import com.sda.andrejko.iss.model.Person;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class People {

    private List<Person> people;
    private Integer number;
    private String message;
}
