package com.sda.andrejko.iss.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PersonDTO {

    @JsonProperty("name")
    private String fullName;
    private String craft;
}
