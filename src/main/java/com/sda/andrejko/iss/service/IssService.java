package com.sda.andrejko.iss.service;

import com.sda.andrejko.iss.model.Person;
import com.sda.andrejko.iss.dto.Position;
import java.util.List;

public interface IssService {

        List<Person> getAstronauts();

        Position getIssPosition();

    }
